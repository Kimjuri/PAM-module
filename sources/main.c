#include <sys/types.h>
#include <string.h>
#include <security/pam_modules.h>
#include <stdio.h>
#include <stdlib.h>
#include <pwd.h>

void	unmount_encfs(char* const		user_dir)
{
  unsigned int	size = 0;
  char*		cmd;
  char* const	cmd_pattern = "fusermount -u %s/secure_data-rw";

  size += strlen(cmd_pattern);
  size += strlen(user_dir);
  size += 1;
  size *= sizeof(*cmd);
  cmd = malloc(size);
  if (cmd)
    {
      sprintf(cmd, cmd_pattern, user_dir);
      system(cmd);
      free(cmd);
    }
}

void	mount_encfs(char* const		user_dir,
		    unsigned int	user_uid,
		    unsigned int	user_gid)
{
  unsigned int	size = 0;
  char*		cmd;
  char* const	cmd_pattern = "encfs -o uid=%d -o gid=%d -o allow_other %s/.secure_data-rw %s/secure_data-rw";

  size += strlen(cmd_pattern);
  size += strlen(user_dir) * 2;
  size += 11;
  size *= sizeof(*cmd);
  cmd = malloc(size);
  if (cmd)
    {
      sprintf(cmd, cmd_pattern, user_uid, user_gid, user_dir, user_dir);
      system(cmd);
      free(cmd);
    }
}

int             is_my_folder_already_mount(char *type, char *path)
{
  FILE* file = fopen("/proc/mounts", "r");
  char line[256];

  if (type == NULL || path == NULL)
    return (0);
  while (fgets(line, sizeof(line), file)) {
    if (!strncmp(line, type, strlen(type)) && line[strlen(type)] == ' ')
      if (!strncmp(&line[strlen(type) + 1], path, strlen(path)) && line[strlen(type) + strlen(path) + 1] == ' ')
        return (1);
  }
  fclose(file);
  return (0);
}

/* expected hook */
PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh,
			      int flags,
			      int argc,
			      const char **argv )
{
  return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh,
				int flags,
				int argc,
				const char **argv)
{
  return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t *pamh,
				   int flags,
				   int args,
				   const char **argv)
{
  return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t *pamh,
				    int flags,
				    int args,
				    const char **argv)
{
  const char*		user_name;
  struct passwd*	user_info;
  char* const		path_pattern = "%s/secure_data-rw";
  char*			path;

  if (pam_get_user(pamh, &user_name, "Username: ") == PAM_SUCCESS)
    {
      user_info = getpwnam(user_name);
      if (user_info)
	{
	  path = malloc(sizeof(*path) * (strlen(user_info->pw_dir) + strlen(path_pattern) + 1));
	  if (path)
	    {
	      sprintf(path, path_pattern, user_info->pw_dir);
	      if (is_my_folder_already_mount("encfs", path) == 1)
		{
		  unmount_encfs(user_info->pw_dir);
		}
	      free(path);
	    }
	}
    }
  return PAM_SUCCESS;
}

/* expected hook, this is where custom stuff happens */
PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh,
				   int flags,
				   int argc,
				   const char **argv)
{
  const char*		user_name;
  struct passwd*	user_info;
  char* const		path_pattern = "%s/secure_data-rw";
  char*			path;

  if (pam_get_user(pamh, &user_name, "Username: ") == PAM_SUCCESS)
    {
      user_info = getpwnam(user_name);
      if (user_info)
	{
	  path = malloc(sizeof(*path) * (strlen(user_info->pw_dir) + strlen(path_pattern) + 1));
	  if (path)
	    {
	      sprintf(path, path_pattern, user_info->pw_dir);
	      if (is_my_folder_already_mount("encfs", path) == 0)
		{
		  mount_encfs(user_info->pw_dir, user_info->pw_uid, user_info->pw_gid);
		}
	      free(path);
	    }
	}
    }
  return PAM_SUCCESS;
}
