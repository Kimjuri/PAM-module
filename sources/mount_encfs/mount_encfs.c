#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	mount_encfs(char* const		user_dir,
		    unsigned int	user_uid,
		    unsigned int	user_gid)
{
  unsigned int	size = 0;
  char*		cmd;
  char* const	cmd_pattern = "encfs -o uid=%d -o gid=%d -o allow_other %s/.secure_data-rw %s/secure_data-rw";

  size += strlen(cmd_pattern);
  size += strlen(user_dir) * 2;
  size += 11;
  size *= sizeof(*cmd);
  cmd = malloc(size);
  if (cmd)
    {
      sprintf(cmd, cmd_pattern, user_uid, user_gid, user_dir, user_dir);
      system(cmd);
      free(cmd);
    }
}

int	main()
{
  mount_encfs("/home/bob", 1000, 1000);
}
