#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

char*	get_user_home_path(char const* user_name);

void	simple_user_test()
{
  char*	bob_home = get_user_home_path("bob");

  assert(bob_home != NULL);
  assert(strcmp(bob_home, "/home/bob") == 0);

  free(bob_home);
}

void	simple_root_test()
{
  char*	root_home = get_user_home_path("root");

  assert(root_home != NULL);
  assert(strcmp(root_home, "/root") == 0);

  free(root_home);
}

int	main()
{
  
  simple_user_test();
  simple_root_test();
  
  return (0);
}
