#include <sys/types.h>
#include <pwd.h>
#include <string.h>

char*	get_user_home_path(char const* user_name)
{
  struct passwd* user_info = getpwnam(user_name);

  if (user_info == NULL)
    {
      return (NULL);
    }
  return (strdup(user_info->pw_dir));
}
