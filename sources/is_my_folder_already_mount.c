#include <stdio.h>
#include <string.h>

int             is_my_folder_already_mount(char *type, char *path)
{
  FILE* file = fopen("/proc/mounts", "r");
  char line[256];

  if (type == NULL || path == NULL)
    return (0);
  while (fgets(line, sizeof(line), file)) {
    if (!strncmp(line, type, strlen(type)) && line[strlen(type)] == ' ')
      if (!strncmp(&line[strlen(type) + 1], path, strlen(path)) && line[strlen(type) + strlen(path) + 1] == ' ')
        return (1);
  }
  fclose(file);
  return (0);
}
