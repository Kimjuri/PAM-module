#include <string.h>
#include <stdlib.h>
#include <unistd.h>

char* get_user_home_path(char* const);
char* append_to_path(char* const, char* const);

int main()
{
  char*		user_name = "bob";
  char*		user_home_path;
  char*		encrypted_path;
  char*		decrypted_path;
  char*		args[8];
  char*		arge[1] = { NULL };

  user_home_path = get_user_home_path(user_name);

  if (user_home_path != NULL)
    {
      encrypted_path = append_to_path(user_home_path, ".secure_data-rw");
      decrypted_path = append_to_path(user_home_path, "secure_data-rw");
      if (encrypted_path && decrypted_path)
	{
	  args[0] = "/usr/bin/encfs";
	  args[1] = "-o";
	  args[2] = "allow_other";
	  args[3] = "-o";
	  args[4] = "nonempty";
	  args[5] = encrypted_path;
	  args[6] = decrypted_path;
	  args[7] = NULL;
	  
	  int suid = fork();
	  if (suid == 0)
	    {
	      printf("MOUNTING\n");
	      execve(args[0], args, arge);
	    }
	  else
	    {
	      wait();
	      printf("MOUNTING DONE\n");
	    }
	  free(encrypted_path);
	  free(decrypted_path);
	}
      free(user_home_path);
    }
}
