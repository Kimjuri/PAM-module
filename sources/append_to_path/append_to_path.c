#include <stdlib.h>
#include <string.h>

char*	append_to_path(char* const path, char* const addon)
{
  char*	res = malloc(sizeof(char) * (strlen(path) + strlen(addon) + 2));

  if (res == NULL)
    {
      return (NULL);
    }
  strcpy(res, path);
  strcat(res, "/");
  strcat(res, addon);
  return (res);
}
