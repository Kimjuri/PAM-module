#include <assert.h>
#include <string.h>
#include <stdlib.h>

char*	append_to_path(char const* path, char const* addon);

void	simple_test()
{
  char const* path = "/home/bob";
  char const* addon = "coffre";

  char *new_path = append_to_path(path, addon);

  assert(new_path != NULL);
  assert(strcmp(new_path, "/home/bob/coffre") == 0);

  free(new_path);
}

void	simple_test_hide()
{
  char const* path = "/home/bob";
  char const* addon = ".coffre";

  char *new_path = append_to_path(path, addon);

  assert(new_path != NULL);
  assert(strcmp(new_path, "/home/bob/.coffre") == 0);

  free(new_path);
}

int	main()
{
  
  simple_test();
  simple_test_hide();
  
  return (0);
}
