NAME		=	mypam.so

SRC		=	sources/main.c

OBJ		=	$(SRC:.c=.o)

CFLAGS		=	-fPIC -fno-stack-protector

LIB		=	-lpam

CC		=	gcc

DEPENDENCIES	=	encfs libpam0g-dev


all:			$(NAME)

$(NAME):		shellInstall $(OBJ)
			ld -x --shared -o $(NAME) $(OBJ) $(LIB)	

shellInstall:
			./shell_script/install_dependencies.sh

shellUninstall:
			./shell_script/uninstall.sh

shellCheck:		./shell_script/check.sh

install:		$(NAME)
			ld -x --shared -o /lib/security/$(NAME) $(OBJ) $(LIB)	

uninstall:	shellUninstall
		@echo uninstall

clean:		shellUninstall
		rm -rf $(OBJ) $(NAME) /lib/security/$(NAME)

check:		shellCheck
		@echo check

test:	
		@echo test

re:		 fclean all
