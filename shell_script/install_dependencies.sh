#!/bin/sh

apt-get -y install encfs libpam0g-dev
if [ -d "/lib/security" ]; then
    echo "Directory /lib/security already exists"
else
   mkdir /lib/security
fi

if ! grep -Fxq "auth sufficient mypam.so" /etc/pam.d/common-auth
   then
       echo "auth sufficient mypam.so" >> /etc/pam.d/common-auth
fi

if ! grep -Fxq "account sufficient mypam.so" /etc/pam.d/common-auth
   then
       echo "account sufficient mypam.so" >> /etc/pam.d/common-auth
fi

if ! grep -Fxq "session sufficient mypam.so" /etc/pam.d/common-auth
   then
       echo "session sufficient mypam.so" >> /etc/pam.d/common-auth
fi

modprobe fuse

if ! grep -Fxq "user_allow_other" /etc/fuse.conf
   then
       echo "user_allow_other" >> /etc/fuse.conf
fi
