#!/bin/sh

if grep -Fxq "auth sufficient mypam.so" /etc/pam.d/common-auth && grep -Fxq "account sufficient mypam.so" /etc/pam.d/common-auth && grep -Fxq "session sufficient mypam.so" /etc/pam.d/common-auth && grep -Fxq "user_allow_other" /etc/fuse.conf && [ -e /lib/security/mypam.so ]
then
    echo "Module pam is installed"
else
    echo "Module pam is not installed"
fi
